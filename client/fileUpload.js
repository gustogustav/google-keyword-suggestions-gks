import { SimpleDropzone } from 'simple-dropzone'

const fileupload = document.getElementById('fileupload')
const fileuploadBtn = document.getElementById('fileuploadBtn')
const inputKeywords = document.getElementById('inputKeywords')
const errorOutput = document.getElementById('errorOutput')

const attachFile = (file, fileName) => {
  if (file) {
    const reader = new FileReader()
    reader.readAsText(file, 'UTF-8')
    reader.onload = (evt) => {
      inputKeywords.innerHTML = evt.target.result
      const lines = evt.target.result.split('\n').length || 0
      errorOutput.innerHTML = `<div class="alert alert-info w-100 my-3" role="alert"><b>Imported ${lines} lines</div>`
    }
    reader.onerror = () => {
      errorOutput.innerHTML = `<div class="alert alert-danger w-100 my-3" role="alert"><b>Error reading: </b> ${fileName}</div>`
    }
  }
}

fileuploadBtn.onclick = () => {
  fileupload.click()
}

// eslint-disable-next-line
const dropCtrl = new SimpleDropzone(document.body, fileupload)
dropCtrl.on('drop', ({ files }) => {
  const fileName = [...files.keys()][0]
  const file = [...files.values()][0]
  attachFile(file, fileName)
})

fileupload.onchange = () => {
  const fileName = fileupload.value.split('\\')[fileupload.value.split('\\').length - 1]
  const file = fileupload.files[0]
  attachFile(file, fileName)
}
