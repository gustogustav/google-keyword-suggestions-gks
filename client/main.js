// Load App's css (scss)
import '@/assets/css/app.scss'

// Import bootstrap js
import 'bootstrap/dist/js/bootstrap.bundle.min'
import 'select2/dist/js/select2'
import './fileUpload'
import * as $ from 'jquery'
import Trianglify from 'trianglify'

import { randomVal, colours } from './utilities'

const inputKeywords = document.getElementById('inputKeywords')
const validate = document.getElementById('validate')
const errorOutput = document.getElementById('errorOutput')

$('[name="reportName"]').val(`Google Keyword Suggestions ${new Date().toJSON().slice(0, 10)}`)

const submitLinks = async () => {
  validate.disabled = true
  $('.loader.small').removeClass('d-none')
  if (inputKeywords.value) {
    const keywords = inputKeywords.value.trim().split('\n')
    fetch('/gks', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        reportName: $('input[name="reportName"]').val(),
        keywords,
        location: $('select[name="location"]').val(),
        analyseTopResults: $('input[name="analyseTopResults"]')[0].checked,
      }),
    }).then(resp => resp.json())
      .then((el) => {
        if (el.success) {
          window.open(el.link, '_blank')
          let notification = `<div class="alert alert-primary w-100" role="alert"><a href='${el.link}'>Click here to open results</a></div>`
          if (el.slidesLink) {
            window.open(el.slidesLink, '_blank')
            notification += `<div class="alert alert-primary w-100" role="alert"><a href='${el.slidesLink}'>Click here to open slides</a></div>`
          }
          errorOutput.innerHTML = notification
        } else {
          errorOutput.innerHTML = `<div class="alert alert-warning w-100" role="alert">${el.message}</div>`
        }
        validate.disabled = false
        $('.loader.small').addClass('d-none')
      }).catch((err) => {
        // eslint-disable-next-line no-console
        console.log(err)
        validate.disabled = false
        $('.loader.small').addClass('d-none')
        errorOutput.innerHTML = '<div class="alert alert-warning w-100" role="alert">Server error. Please contact the katanya team</div>'
      })
  } else {
    errorOutput.innerHTML = '<div class="alert alert-warning w-100" role="alert">Please enter some keywords</div>'
    validate.disabled = false
    $('.loader.small').addClass('d-none')
  }
}
validate.addEventListener('click', submitLinks)

const backgroundColour = colours[randomVal(0, colours.length)]
const addBackground = async () => {
  const pattern = Trianglify({
    width: window.innerWidth,
    height: window.innerHeight,
    variance: 1,
    cell_size: 150,
    x_colors: ['#fff', '#fff', '#fff', backgroundColour],
  })
  document.body.appendChild(pattern.canvas())
}

window.onload = async () => { addBackground() }
let timeout = false
window.addEventListener('resize', () => {
  if (!timeout) {
    timeout = setTimeout(() => {
      document.querySelector('canvas').remove()
      addBackground()
      timeout = false
    }, 1000)
  }
})
