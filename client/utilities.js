
const randomVal = (min, max) => {
  return Math.floor(Math.random() * (max - min) + 1) + min
}

// function isValidURL(str) {

// }

const colours = ['#ec1c66', '#ef4460', '#f47d71', '#f8a581', '#f1c9b8',
  '#e7ba69', '#4c0341', '#770f42', '#bf2353', '#e8667b', '#a40500',
  '#282562', '#66627e', '#6d1e6a', '#91288d', '#072647', '#0e4c8d',
  '#4d91da', '#005f83', '#029d99', '#18b2be', '#2bdad1', '#9dcfc2',
  '#6c542f', '#696140', '#8f7837', '#6d826f', '#989d92', '#5d6d30',
  '#929237', '#b4a132', '#c2ce6a', '#deca5a', '#005b34', '#8bc53f',
  '#bed63a', '#d42128', '#e94600', '#f79c1d', '#ffb717', '#d79928',
  '#af6029', '#3b2416', '#5f342c', '#5a4a42', '#726758', '#9b8579', '#c2b59c']

// eslint-disable-next-line import/prefer-default-export
export { randomVal, colours }
