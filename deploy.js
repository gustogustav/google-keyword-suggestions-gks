const { deploy } = require('sftp-sync-deploy');
const homedir = require('os').homedir();

let config = {
  host: '52.38.241.116',           
  port: 22,
  username: 'ec2-user',               
  privateKey: homedir + '/Documents/katanyalinuxdev.pem',
  localDir: 'dist',            
  remoteDir: '/home/ec2-user/sites/gks'      
};
 
let options = {
  exclude: [
    'node_modules',
    'logs',
  ],
  excludeMode: 'ignore', 
};

deploy(config, options).then(() => {
  console.log('success!');
}).catch(err => {
  console.error('error! ', err);
})