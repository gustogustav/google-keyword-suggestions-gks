/* eslint-disable camelcase */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-cond-assign */
/* eslint-disable radix */
/* eslint-disable no-undef */
import express from 'express'
import helmet from 'helmet'
import bodyParser from 'body-parser'
import fs from 'fs'
import path from 'path'
import readline from 'readline'
import { google } from 'googleapis'
import jwt from 'jsonwebtoken'
import cookieParser from 'cookie-parser'
import throttledQueue from 'throttled-queue'
import cors from 'cors'
import { AdwordsUser, AdwordsAuth } from 'node-adwords'

require('events').EventEmitter.defaultMaxListeners = 0

process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0
process.setMaxListeners(500)

const app = express()
const port = process.env.PORT || 4200
app.disable('x-powered-by')
app.disable('etag')
app.use(helmet())
app.use(bodyParser.json({ limit: '20mb' }))
app.use(cookieParser(process.env.SESSION_SECRET))
app.use(cors())

const validatedJWT = (req, res, next) => {
  const { token } = req.cookies
  if (!token) {
    return res.redirect('/auth')
  }
  jwt.verify(token, process.env.SESSION_SECRET, (err, decodedToken) => {
    if (err) {
      res.redirect('/auth')
    } else
    if (decodedToken && decodedToken.split('@')[1] === 'katanya.co.uk') {
      next()
    } else {
      res.redirect('/auth')
    }
  })
}

// app.use(validatedJWT)

const mainSheet = 'Google keywords'

// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/presentations', 'https://www.googleapis.com/auth/drive']
const TOKEN_PATH = 'token.json'

function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  })
  // eslint-disable-next-line no-console
  console.log('Authorize this app by visiting this url:', authUrl)
  if (process.env.SLACK_BOT_TOKEN) {
    // eslint-disable-next-line global-require
    require('slack').chat.postMessage({ token: process.env.SLACK_BOT_TOKEN, text: `Authorize gst app by visiting this url: ${authUrl}`, channel: process.env.SLACK_BOT_CHANNEL || 'UC336623E' })
  } else {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    })
    rl.question('Enter the code from that page here: ', (code) => {
      rl.close()
      oAuth2Client.getToken(code, (err, token) => {
        if (err) return s('Error retrieving access token', err)
        oAuth2Client.setCredentials(token)

        // Store the token to disk for later program executions
        fs.writeFile(TOKEN_PATH, JSON.stringify(token), (er) => {
          if (er) return s(er)
          console.log('Token stored to', TOKEN_PATH)
          return true
        })
        return callback(oAuth2Client)
      })
    })
  }
}


/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  // eslint-disable-next-line camelcase
  const { client_secret, client_id, redirect_uris } = credentials.installed
  const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0])

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback)

    oAuth2Client.setCredentials(JSON.parse(token))
    return callback(oAuth2Client)
  })
}


let sheets = null
let drive = null
let slides = null
let folderId = '1C3YmJUyA1sNs49-N1QR_ppfo3xFuWOYE'

const startSheets = async (auth) => {
  sheets = google.sheets({ version: 'v4', auth })
  drive = google.drive({ version: 'v3', auth })
  slides = google.slides({ version: 'v1', auth })

  if (!folderId) {
    const folder = await drive.files.create({
      resource: {
        name: 'seo reports',
        mimeType: 'application/vnd.google-apps.folder',
      },
      fields: 'id',
    })
    folderId = folder.data.id
  }
}

const adWords = new AdwordsUser({
  developerToken: process.env.DEVELOPER_TOKEN_ADWORDS || 'h5pLyvTSgekJ6M3rLfa5Iw', //your adwords developerToken
  userAgent: process.env.COMPANY_NAME_ADWORDS || 'Katanya', //any company name
  clientCustomerId: process.env.CLIENT_CUSTOMER_ID_ADWORDS || '159-678-2747', //the Adwords Account id (e.g. 123-123-123)
  refresh_token: '1//0g_sJq2gr-floCgYIARAAGBASNwF-L9IrGunknsjjXq-nRozxiLSJv3qw0h5POIAb1LpQ1fnZmuPRCibKPJ6Y8AmQRebCja8tOsY',
  client_id: '1008460468561-oeh9ak1m2dhqr26oc1lktquh90u91krv.apps.googleusercontent.com',
  client_secret: 'lD5kRvNpPSl3QCU19r5N9kHs'
});
const TargetingIdeaService = adWords.getService('TargetingIdeaService', 'v201809')

let auth = new AdwordsAuth({
  client_id: '1008460468561-oeh9ak1m2dhqr26oc1lktquh90u91krv.apps.googleusercontent.com', //this is the api console client_id
  client_secret: 'lD5kRvNpPSl3QCU19r5N9kHs'
}, 'http://localhost:4200/adwords/auth' /** insert your redirect url here */);

//assuming express
app.get('/adwords/go', (req, res) => {
  res.redirect(auth.generateAuthenticationUrl());
})

app.get('/adwords/auth', (req, res) => {
  auth.getAccessTokenFromAuthorizationCode(req.query.code, (error, tokens) => {
      //save access and especially the refresh tokens here
      console.log('error', error);
     console.log('tokens', tokens);
  })
});

// Load client secrets from a local file.
const credentials = {
  installed: {
    client_id: '10565604851-ec4ckdrqf2qb0kkqj16ml45hidqscpoj.apps.googleusercontent.com',
    project_id: 'quickstart-1568608244422',
    auth_uri: 'https://accounts.google.com/o/oauth2/auth',
    token_uri: 'https://oauth2.googleapis.com/token',
    auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
    client_secret: 'KK_ZEw9od19ppLpy9JALx_6e',
    redirect_uris: ['urn:ietf:wg:oauth:2.0:oob', 'http://localhost'],
  },
}

authorize(credentials, startSheets)

const concat = (x, y) => x.concat(y)

const toColumnName = (num) => {
  let ret = ''
  for (let a = 1, b = 26; (num -= a) >= 0; a = b, b *= 26) {
    ret = String.fromCharCode(parseInt((num % b) / a) + 65) + ret
  }
  return ret
}

const throttle = throttledQueue(1, 5000)

const run = async (body, spreadsheetId,) => {
  const drawCell = async (value, row, coll, sheetName = mainSheet) => {
    sheets.spreadsheets.values.update({
      spreadsheetId,
      range: `${sheetName}!${toColumnName(coll)}${row}`,
      valueInputOption: 'RAW',
      resource: {
        values: [[value]],
      },
    })
  }


  let batchUpdate = []
  let total = 0
  const drawRow = (array, sheetName = mainSheet) => {
    batchUpdate.push(array)
    total += 1
    if (batchUpdate.length >= 40 || total > body.keywords.length) {
      throttle(() => {
        try {
          sheets.spreadsheets.values.append({
            spreadsheetId,
            range: sheetName,
            valueInputOption: 'USER_ENTERED',
            insertDataOption: 'INSERT_ROWS',
            resource: {
              values: batchUpdate,
            },
          })
          batchUpdate = []
          drawCell(`${total} of ${body.urls.length}`, 1, 1, sheetName)
          // perform some type of activity in here.
        } catch (err) {
          console.trace(err)
        }
      })
    }
  }

  await sheets.spreadsheets.batchUpdate({
    spreadsheetId,
    resource: {
      requests: [
        {
          addSheet: {
            properties: {
              title: mainSheet,
            },
          },
        },
        {
          deleteSheet: {
            sheetId: 0,
          },
        },
      ],
    },
  })

  drawCell(`0 of ${body.keywords.length}`, 1, 1)

  let selector = {
    'ideaType': 'KEYWORD',
    'requestType': 'IDEAS',
    'requestedAttributeTypes': [
      'KEYWORD_TEXT', 'SEARCH_VOLUME', 'CATEGORY_PRODUCTS_AND_SERVICES'
    ],
    'paging': {
      'startIndex': '0',
      'numberResults': '100',
    },
    'searchParameters': [{
      'xsi_type': 'RelatedToQuerySearchParameter',
      'queries': ['space cruise']
    },{
      'xsi_type': 'LanguageSearchParameter',
      'languages': [{'id': '1000'}]
    },{
      'xsi_type': 'NetworkSearchParameter',
      'networkSetting': {
          'targetGoogleSearch': True,
          'targetSearchNetwork': False,
          'targetContentNetwork': False,
          'targetPartnerSearchNetwork': False
      }
    }],
  }

  // targetingIdeaService.get({selector: selector}, (error, result) => {
  //   console.log('error', error);
  //   console.log('result', result);
  //   // drawRow(result)
  // })

  drawCell(body.reportName, 1, 1)
}


app.get('*', (req, res) => {
  // res.send('hey')
  if (process.env.NODE_ENV !== 'production') {
    console.log('loading dev client')
    const homedir = require('os').homedir()

    return res.sendFile(`${homedir}/Documents/projects/Google Keyword Suggestions (GKS)/dist/index.html`)
  }
  return res.sendFile(path.join('/home/ec2-user/sites/gks/index.html'))
})


app.post('*', async (req, res) => {
  req.setTimeout(0)
  // console.log(req.body)
  if (req.body.keywords && Array.isArray(req.body.keywords)) {
    try {
      const sheet = await sheets.spreadsheets.create({
        fields: 'spreadsheetId',
        resource: {
          properties: {
            title: req.body.reportName || `Google Keyword Suggestions  ${new Date().toJSON().slice(0, 10)}`,
          },
        },
      })

      const spreadsheetId = sheet.data.spreadsheetId
      await drive.permissions.create({
        fileId: spreadsheetId,
        parents: [folderId],
        resource: {
          role: 'reader',
          type: 'anyone',
        },
      })
      const webViewLink = await drive.files.get({
        fileId: spreadsheetId,
        fields: 'webViewLink',
      })

      const link = webViewLink.data.webViewLink
      // console.log(link)
      run(req.body, spreadsheetId)

      res.send({ success: true, link })
    } catch (e) {
      console.log(e)
      return res.send({ success: false, message: 'Error creating spreadsheet' })
    }
  } else {
    return res.send({ success: false, message: 'No keywords were provided.' })
  }
})

// https://github.com/ChrisAlvares/node-adwords/blob/master/test/adwords/services/targetingideaservice.js
let selector = {
  searchParameters: [{
      'xsi:type': 'RelatedToQuerySearchParameter',
      queries: ['test']
  }, {
      'xsi:type': 'LanguageSearchParameter',
      languages: [{'cm:id': 1000}]
  }],
  ideaType: 'KEYWORD',
  requestType: 'IDEAS',
  requestedAttributeTypes: ['KEYWORD_TEXT'],
  paging: {
      startIndex: 0,
      numberResults: 20
  },
}

// TargetingIdeaService.get({selector}, (error, result) => {
  // console.log('error', error);
  // console.log('result', result);
  // drawRow(result)
// })

app.listen(port, () => console.log(`GKS api started on port: ${port}`))
if (process.env.SLACK_BOT_TOKEN) {
  // eslint-disable-next-line global-require
  require('slack').chat.postMessage({ token: process.env.SLACK_BOT_TOKEN, text: `Restarted GKS 🐱‍🐉‍ ${process.pid} | ${port}`, channel: process.env.SLACK_BOT_CHANNEL || 'UC336623E' })
}
