const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const PurgecssPlugin = require('purgecss-webpack-plugin')
const glob = require('glob')

const env = process.env.NODE_ENV || 'production'

const config = {
  entry: [
    '@babel/polyfill', path.resolve(__dirname, 'client', 'main.js'),
  ],
  output: {
    path: path.resolve(__dirname, 'build'),
    publicPath: (env === 'development') ? '/' : '/',
    filename: (env === 'development') ? '[name].[hash].js' : '[name].[contenthash].js',
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        shared: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          enforce: true,
          chunks: 'all',
        },
      },
    },
    minimizer: (env !== 'development') ? [
      new OptimizeCSSAssetsPlugin(),
      new TerserPlugin(),
    ] : [
    ],
  },
  mode: env,
  devtool: (env === 'development') ? 'cheap-module-eval-source-map' : undefined,
  devServer: {
    historyApiFallback: true,
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['eslint-loader'],
      },
      {
        test: /\.(scss|css)$/,
        use: [
          // For hot reload in dev https://github.com/webpack-contrib/mini-css-ezxtract-plugin/issues/34
          (env === 'development') ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(png|jpg|gif|otf)$/,
        use: [{
          loader: 'file-loader',
          options: {},
        }],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin('build', {}),
    new HtmlWebpackPlugin({
      title: 'SKT',
      template: path.resolve(__dirname, 'client', 'index.html'),
      inject: true,
      minify: (env === 'development') ? undefined : {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true,
      },
      inlineSource: '.(js|css)$', // embed all javascript and css inline
    }),
    new MiniCssExtractPlugin({
      filename: (env === 'development') ? '[name].css' : '[name].[hash].css',
      chunkFilename: (env === 'development') ? '[id].css' : '[id].[hash].css',
    }),
    new HtmlWebpackInlineSourcePlugin(),
  ],
  resolve: {
    extensions: ['.js', '.json'],
    alias: {
      '@': path.resolve(__dirname, 'client'),
    },
  },
  
}

module.exports = config
